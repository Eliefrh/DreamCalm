[![fr](https://img.shields.io/badge/lang-fr-green.svg)](https://github.com/Eliefrh/DreamCalm/blob/main/README.fr.md)
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/Eliefrh/DreamCalm/blob/60ed4ec1c9a043072405657deda0b08a98ed18b5/baby-sleep-sounds-master/app/src/main/music_notes-playstore.png">
    <img src="baby-sleep-sounds-master/app/src/main/music_notes-playstore.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Dream Calm</h3>

  <p align="center">
    Sweet dreams for your little one!
  </p>
  <a href="https://f-droid.org/packages/protect.babysleepsounds/" target="_blank">
<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="90"/></a>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

![image](https://github.com/Eliefrh/DreamCalm/assets/78127649/8b16133d-eb0f-4d40-9530-0ef896fe7dfa)



Dream Calm is an application designed to create a soothing environment for babies by providing a selection of sounds, including white noise and nature sounds. Additionally, users have the ability to add their own sounds to the app, which will play in a loop.

### Features
* Sound Selection: Choose from a variety of sounds, including white noise and nature sounds.
* Animated GIF Icons: Enjoy animated icons for a visually engaging experience.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/4f39dc56-1c1f-440e-b100-f6becbe8ba00" width="300"><br>

* Portrait and Landscape Support: The app supports both portrait and landscape orientations.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/7e1db431-9c8c-4855-95d2-874ff9e48f6e" width="300">
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/9822f3b3-d882-48a4-b433-6298d83a5945" height="300"><br>

* Dark and Light Mode: Switch between dark and light mode for comfortable viewing in any lighting condition.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/c48bdb7a-de66-43cb-b30c-4efd1ac2e6d7" width="300">
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/d71b4895-42c8-486a-a1b9-5203daac30ee" width="300"><br>

* Multilingual Support: The app is available in both French and English for a wider audience.
* Custom Sound Addition: Users can add their own audio files to the app for playback.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/f899472b-f631-4c1d-9e50-f25eb27157c8" width="300"><br>

* Timer Functionality: Set a timer for sound playback to automatically stop after a specified duration.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/058a5b70-4f2a-4d7c-bbde-03af062bcd7a" width="300"><br>

* Bluetooth Control: Control sound playback using Bluetooth devices for added convenience.
* Frequency Adjustment: Change the frequency of sounds to suit your preferences.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/0aaabe49-1e36-46b6-9b9e-3f4aedd58c27" width="300"><br>


<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

* [![Kotlin][Kotlin.com]][Kotlin-url]
* [![Fdroid][Fdroid.com]][Fdroid-url]
* [![FFmpeg][FFmpeg.com]][FFmpeg-url]
* [![Android][Android.com]][Android-url]
* [![Gradle][Gradle.com]][Gradle-url]
<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started


### Prerequisites

Before proceeding, ensure you have the following prerequisites installed:

* [Kotlin](https://kotlinlang.org/)
* [Gradle](https://gradle.org/)
* [Android Studio](https://developer.android.com/studio?gad_source=1&gclid=CjwKCAjwouexBhAuEiwAtW_Zx4rcwAaf8bnnLfGrwS3j49WSuDIwCBfkXb4W3YiNxSyPw86H3IouxhoCe-IQAvD_BwE&gclsrc=aw.ds) (optional, but recommended for Android development)

### Installation

Follow these steps to install and set up your Kotlin project:

1. Clone the repository
 ```sh
git clone https://github.com/Eliefrh/DreamCalm.git
  ```
2. Open the project in your preferred IDE or text editor.
3. Build the project using Gradle.
```sh
gradle build
 ```
4. If required, configure any project-specific settings in the build.gradle or other configuration files.
5. Run the project on an Android device.
```sh
gradle run
 ```

### Documentation
To generate HTML documentation using Gradle Dokka, execute the (`gradle dokkahtml`)


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the GPLv3 License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

Dream Calm is developed by [Narges](https://github.com/NarguSabz) and [Elie](https://github.com/Eliefrh), inspired by the [Baby sleep sounds](https://github.com/brarcher/baby-sleep-sounds) app by [Branden Archer](https://github.com/brarcher).

The following sound resources were created by [Canton Becker](http://whitenoise.cantonbecker.com)
and are distributed under the CC BY-NC-SA 3.0 License:
 - noise_only.mp3

The following sound resources were created by [MC2Method.org](http://mc2method.org/white-noise/)
and are distributed under the condition of "Personal Use":
  - dryer.mp3
  - fan.mp3
  - ocean.mp3
  - rain.mp3
  - refrigerator.mp3
  - shower.mp3
  - stream.mp3
  - vacuum.mp3
  - water.mp3
  - waterfall.mp3
  - waves.mp3

[Campfire-1.mp3](https://www.soundjay.com/nature/campfire-1.mp3) Copyright SoundJay.com, used with Permission.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[Kotlin-url]: https://kotlinlang.org/
[Kotlin.com]: https://img.shields.io/badge/Kotlin-white?logo=Kotlin&link=https%3A%2F%2Fkotlinlang.org%2F 
[Fdroid-url]: https://f-droid.org/en/
[Fdroid.com]: https://img.shields.io/badge/F--Droid-blue?logo=Fdroid&link=https%3A%2F%2Ff-droid.org%2Fen%2F
[FFmpeg-url]: https://ffmpeg.org/
[FFmpeg.com]: https://img.shields.io/badge/FFmpeg-white?logo=ffmpeg&logoColor=green&link=https%3A%2F%2Fffmpeg.org%2F
[Android-url]: https://developer.android.com/studio?gad_source=1&gclid=CjwKCAjwouexBhAuEiwAtW_ZxwWbcQbTlq7LgRBNaHZr1O1bXYFwxNLvTfp9L1xw85MFZzy_DUQCGBoC9Q4QAvD_BwE&gclsrc=aw.ds
[Android.com]: https://img.shields.io/badge/Android%20Studio%20-%20green?logo=android%20studio&logoColor=white&link=https%3A%2F%2Fdeveloper.android.com%2Fstudio%3Fgad_source%3D1%26gclid%3DCjwKCAjwouexBhAuEiwAtW_ZxwWbcQbTlq7LgRBNaHZr1O1bXYFwxNLvTfp9L1xw85MFZzy_DUQCGBoC9Q4QAvD_BwE%26gclsrc%3Daw.ds
[Gradle-url]: https://ffmpeg.org/](https://gradle.org/
[Gradle.com]:https://img.shields.io/badge/Gradle%20-%20white?logo=gradle&logoColor=green&labelColor=white&color=gray&link=https%3A%2F%2Fgradle.org%2F


