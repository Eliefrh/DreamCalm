[![en](https://img.shields.io/badge/lang-en-red.svg)](https://github.com/Eliefrh/DreamCalm/blob/main/README.md)
<!-- LOGO DU PROJET -->
<br />
<div align="center">
  <a href="https://github.com/Eliefrh/DreamCalm/blob/60ed4ec1c9a043072405657deda0b08a98ed18b5/baby-sleep-sounds-master/app/src/main/music_notes-playstore.png">
    <img src="baby-sleep-sounds-master/app/src/main/music_notes-playstore.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Dream Calm</h3>

  <p align="center">
    Des nuits paisibles pour votre tout-petit!
  </p>
  <a href="https://f-droid.org/packages/protect.babysleepsounds/" target="_blank">
    <img src="https://f-droid.org/badge/get-it-on.png" alt="Obtenez-le sur F-Droid" height="90"/>
  </a>
</div>

<!-- TABLE DES MATIÈRES -->
<details>
  <summary>Table des matières</summary>
  <ol>
    <li><a href="#à-propos-du-projet">À propos du projet</a></li>
    <ul>
      <li><a href="#réalisé-avec">Réalisé avec</a></li>
    </ul>
    <li><a href="#pour-commencer">Pour commencer</a></li>
    <ul>
      <li><a href="#prérequis">Prérequis</a></li>
      <li><a href="#installation">Installation</a></li>
    </ul>
    <li><a href="#contribution">Contribution</a></li>
    <li><a href="#licence">Licence</a></li>
    <li><a href="#reconnaissances">Reconnaissances</a></li>
  </ol>
</details>

<!-- À PROPOS DU PROJET -->
## À propos du projet

![image](https://github.com/Eliefrh/DreamCalm/assets/78127649/c1a0d9f7-1e30-4908-b620-c37f327da1c5)


Dream Calm est une application conçue pour créer un environnement apaisant pour les bébés en proposant une sélection de sons, y compris du bruit blanc et des sons de la nature. De plus, les utilisateurs ont la possibilité d'ajouter leurs propres sons à l'application, qui seront lus en boucle.


### Fonctionnalités
* Sélection de sons : Choisissez parmi une variété de sons, y compris du bruit blanc et des sons de la nature.
* Icônes GIF animées : Profitez d'icônes animées pour une expérience visuelle attrayante.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/2cb56265-64fb-427a-8ca5-8ecbfc1c7e63" width="300"><br>

* Support en mode portrait et paysage : L'application prend en charge les orientations portrait et paysage.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/2714bddb-8ac8-4fa0-8640-016e1221933e" width="300">
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/2ad750c2-68a1-4238-90ef-b425e670a235" height="300"><br>

* Mode sombre et mode clair : Basculez entre le mode sombre et le mode clair pour une visualisation confortable dans toutes les conditions d'éclairage.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/41c3f0f6-5249-487a-aed8-3d53593d880a" width="300">
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/66bf7ab9-ff07-46de-8ea5-868b6ba19f4f" width="300"><br>

* Support multilingue : L'application est disponible en français et en anglais pour un public plus large.
* Ajout de son personnalisé : Les utilisateurs peuvent ajouter leurs propres fichiers audio à l'application pour la lecture.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/91a30c82-fd08-4b2c-b04f-5b03fa825829" width="300"><br>

* Fonctionnalité de minuterie : Définissez une minuterie pour l'arrêt automatique de la lecture des sons après une durée spécifiée.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/6883f245-903f-44d4-9945-ff3c25159626" width="300"><br>

* Contrôle Bluetooth : Contrôlez la lecture des sons à l'aide de périphériques Bluetooth pour plus de commodité.
* Ajustement de la fréquence : Changez la fréquence des sons selon vos préférences.<br>
<img src="https://github.com/Eliefrh/DreamCalm/assets/78127649/3f9620ef-6978-4cc1-9223-8cc57fb0c634" width="300"><br>


<p align="right">(<a href="#readme-top">retour en haut</a>)</p>

### Réalisé avec

* [![Kotlin][Kotlin.com]][Kotlin-url]
* [![Fdroid][Fdroid.com]][Fdroid-url]
* [![FFmpeg][FFmpeg.com]][FFmpeg-url]
* [![Android][Android.com]][Android-url]
* [![Gradle][Gradle.com]][Gradle-url]
<p align="right">(<a href="#readme-top">retour en haut</a>)</p>

<!-- POUR COMMENCER -->
## Pour commencer

### Prérequis

Avant de commencer, assurez-vous d'avoir les prérequis suivants installés :

* [Kotlin](https://kotlinlang.org/)
* [Gradle](https://gradle.org/)
* [Android Studio](https://developer.android.com/studio?gad_source=1&gclid=CjwKCAjwouexBhAuEiwAtW_ZxwWbcQbTlq7LgRBNaHZr1O1bXYFwxNLvTfp9L1xw85MFZzy_DUQCGBoC9Q4QAvD_BwE&gclsrc=aw.ds) (facultatif, mais recommandé pour le développement Android)

### Installation

Suivez ces étapes pour installer et configurer votre projet Kotlin :

1. Clonez le dépôt
 ```sh
git clone https://github.com/Eliefrh/DreamCalm.git
  ```
2. Ouvrez le projet dans votre IDE ou éditeur de texte préféré.
3. Construisez le projet en utilisant Gradle.
```sh
gradle build
 ```
4. Si nécessaire, configurez les paramètres spécifiques au projet dans les fichiers build.gradle ou autres fichiers de configuration.
5. Exécutez le projet sur un appareil Android.
```sh
gradle run
 ```

### Documentation
To generate HTML documentation using Gradle Dokka, execute the (`gradle dokkahtml`)

<p align="right">(<a href="#readme-top">retour en haut</a>)</p>

<!-- CONTRIBUTING -->
## Contribution

Les contributions sont ce qui rend la communauté open source si incroyable, un lieu d'apprentissage, d'inspiration et de création. Toutes les contributions que vous apportez sont grandement appréciées.

Si vous avez une suggestion qui pourrait améliorer ce projet, veuillez cloner le dépôt et créer une pull request. Vous pouvez également simplement ouvrir un problème avec le tag "amélioration".
N'oubliez pas de donner une étoile au projet! Merci encore!

1. Faites un fork du projet
2. Créez votre branche de fonctionnalité (git checkout -b feature/AmazingFeature)
3. Validez vos modifications (git commit -m 'Add some AmazingFeature')
4. Poussez vers la branche (git push origin feature/AmazingFeature)
5. Ouvrez une Pull Request

<p align="right">(<a href="#readme-top">retour en haut</a>)</p>

<!-- LICENSE -->
## Licence
Distribué sous la licence GPLv3. Consultez LICENSE.txt pour plus d'informations.

<p align="right">(<a href="#readme-top">retour en haut</a>)</p>

<!-- REMERCIEMENTS -->
## Reconnaissances

Dream Calm est développé par [Narges](https://github.com/NarguSabz) et [Elie](https://github.com/Eliefrh), inspiré par l'application [Baby sleep sounds](https://github.com/brarcher/baby-sleep-sounds) de [Branden Archer](https://github.com/brarcher).

Les ressources sonores suivantes ont été créées par [Canton Becker](http://whitenoise.cantonbecker.com)
et sont distribuées sous la licence CC BY-NC-SA 3.0 :
 - noise_only.mp3

Les ressources sonores suivantes ont été créées par [MC2Method.org](http://mc2method.org/white-noise/)
et sont distribuées sous condition d' "Utilisation Personnelle" :

  - dryer.mp3
  - fan.mp3
  - ocean.mp3
  - rain.mp3
  - refrigerator.mp3
  - shower.mp3
  - stream.mp3
  - vacuum.mp3
  - water.mp3
  - waterfall.mp3
  - waves.mp3

[Campfire-1.mp3](https://www.soundjay.com/nature/campfire-1.mp3) Droits d'auteur SoundJay.com, utilisé avec permission.

<p align="right">(<a href="#readme-top">retour en haut</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[Kotlin-url]: https://kotlinlang.org/
[Kotlin.com]: https://img.shields.io/badge/Kotlin-white?logo=Kotlin&link=https%3A%2F%2Fkotlinlang.org%2F 
[Fdroid-url]: https://f-droid.org/en/
[Fdroid.com]: https://img.shields.io/badge/F--Droid-blue?logo=Fdroid&link=https%3A%2F%2Ff-droid.org%2Fen%2F
[FFmpeg-url]: https://ffmpeg.org/
[FFmpeg.com]: https://img.shields.io/badge/FFmpeg-white?logo=ffmpeg&logoColor=green&link=https%3A%2F%2Fffmpeg.org%2F
[Android-url]: https://developer.android.com/studio?gad_source=1&gclid=CjwKCAjwouexBhAuEiwAtW_ZxwWbcQbTlq7LgRBNaHZr1O1bXYFwxNLvTfp9L1xw85MFZzy_DUQCGBoC9Q4QAvD_BwE&gclsrc=aw.ds
[Android.com]: https://img.shields.io/badge/Android%20Studio%20-%20green?logo=android%20studio&logoColor=white&link=https%3A%2F%2Fdeveloper.android.com%2Fstudio%3Fgad_source%3D1%26gclid%3DCjwKCAjwouexBhAuEiwAtW_ZxwWbcQbTlq7LgRBNaHZr1O1bXYFwxNLvTfp9L1xw85MFZzy_DUQCGBoC9Q4QAvD_BwE%26gclsrc%3Daw.ds
[Gradle-url]: https://ffmpeg.org/](https://gradle.org/
[Gradle.com]:https://img.shields.io/badge/Gradle%20-%20white?logo=gradle&logoColor=green&labelColor=white&color=gray&link=https%3A%2F%2Fgradle.org%2F
